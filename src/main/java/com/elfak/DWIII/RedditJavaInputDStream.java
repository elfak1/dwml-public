package com.elfak.DWIII;

import net.dean.jraw.models.Submission;
import org.apache.spark.streaming.api.java.JavaReceiverInputDStream;
import org.apache.spark.streaming.dstream.ReceiverInputDStream;

import scala.reflect.ClassTag$;

public class RedditJavaInputDStream extends JavaReceiverInputDStream<Submission> {
    public RedditJavaInputDStream(ReceiverInputDStream<Submission> receiverInputDStream) {
        super(receiverInputDStream, ClassTag$.MODULE$.apply(String.class));
    }
}
