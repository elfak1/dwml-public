package com.elfak.DWIII;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceTransactionManagerAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;

import java.lang.reflect.Executable;
import java.util.Iterator;


@SpringBootApplication(exclude = {
		DataSourceAutoConfiguration.class,
		DataSourceTransactionManagerAutoConfiguration.class,
		HibernateJpaAutoConfiguration.class
})
public class DwiiiApplication {


		public static void main(String[] args) {
			try{
			SpringApplication.run(DwiiiApplication.class, args);

		}catch(Exception e) {
			System.out.println(e.getMessage());
		}
		}



}
