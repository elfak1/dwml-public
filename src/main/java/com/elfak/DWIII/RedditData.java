package com.elfak.DWIII;

import net.dean.jraw.models.EmbeddedMedia;
import java.io.Serializable;

public class RedditData implements Serializable {
	public Integer CommentCount;
	public Integer Score;
	public String UniqueId;
	public String PostHint;

	public RedditData(Integer cc, Integer sc, String ui, String ph) {
		this.CommentCount = cc;
		this.Score = sc;
		this.UniqueId = ui;
		this.PostHint = ph;
	}

	@Override
	public String toString() {
		return this.UniqueId + "\n" + this.Score + "\n" + this.CommentCount + "\n"  + this.PostHint + "\n";
	}
}
