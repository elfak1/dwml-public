package com.elfak.DWIII;

import com.elfak.DWIII.RedditClient.Constants;
import com.elfak.DWIII.RedditClient.GlobalRedditClient;
import net.dean.jraw.models.Listing;
import net.dean.jraw.models.Submission;
import net.dean.jraw.models.SubredditSort;
import net.dean.jraw.pagination.DefaultPaginator;
import org.apache.spark.storage.StorageLevel;
import org.apache.spark.streaming.receiver.Receiver;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.*;

import static com.elfak.DWIII.RedditClient.Constants.*;

public class RedditReceiver extends Receiver<Submission> {

    public RedditReceiver(StorageLevel storageLevel) {
        super(storageLevel);
    }
    public List<Submission> getTrending() {
        String name = map.get(TARGET_SUBREDDIT);
        DefaultPaginator.Builder<Submission, SubredditSort> trending = GlobalRedditClient.getClient().subreddit(name).posts();
        Iterator<Listing<Submission>> iterator = trending.build().iterator();

        return iterator.next();
    }
    private void receive()  {
        try {
            while (!isStopped()) {
                getTrending().forEach(this::store);
                //Thread.sleep(1000);
            }
        } catch (Throwable e){
            e.printStackTrace();
        }

    }
    @Override
    public void onStart() {
        new Thread(this::receive).start();
    }

    @Override
    public void onStop() {

    }
}
