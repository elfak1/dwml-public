package com.elfak.DWIII.RedditClient;

import com.elfak.DWIII.RedditData;
import com.elfak.DWIII.RedditInputDReceiver;

import com.elfak.DWIII.RedditJavaInputDStream;
import com.elfak.DWIII.RedditReceiver;
import net.dean.jraw.models.Submission;
import org.apache.spark.SparkConf;

import org.apache.spark.api.java.function.Function;
import org.apache.spark.storage.StorageLevel;
import org.apache.spark.streaming.Duration;
import org.apache.spark.streaming.api.java.JavaDStream;
import org.apache.spark.streaming.api.java.JavaPairDStream;
import org.apache.spark.streaming.api.java.JavaStreamingContext;
import org.springframework.stereotype.Component;

import scala.Function1;
import scala.Tuple2;
import scala.reflect.ClassTag$;


import java.util.*;

@Component
public class JRAWClientWrapper implements IRedditClient {

    public JRAWClientWrapper() {

    }

    @Override
    public void getStream() throws InterruptedException {
        SparkConf conf = new SparkConf().setMaster("local[2]").setAppName("SparkRedditExample");
        JavaStreamingContext streamingContext = new JavaStreamingContext(conf, new Duration(1000));
        RedditJavaInputDStream posts = new RedditJavaInputDStream(new RedditInputDReceiver(streamingContext,  ClassTag$.MODULE$.apply(Submission.class)));

        RedditDataStream(posts);
        streamingContext.start();
        streamingContext.awaitTermination();
    }

    public void RedditDataStream(JavaDStream<Submission> stream) {
        JavaDStream<RedditData> rd = stream.map(x -> new RedditData(x.getCommentCount(), x.getScore(), x.getUniqueId(), x.getPostHint()));
        JavaDStream<String> mediaTypes = rd.map(redditData -> {
                if (redditData.PostHint == null || redditData.PostHint.equals("self"))
                    return "Text";
                if (redditData.PostHint.equals("image"))
                    return "Image";
                else if (redditData.PostHint.equals("link"))
                    return "Link";
                else if (redditData.PostHint.contains("video"))
                    return "Video";
                else
                    return "Unknown";
        });
        JavaPairDStream<String, Integer> single = mediaTypes.mapToPair(mt -> new Tuple2<>(mt, 1));
        JavaPairDStream<String, Integer> typeCount = single.reduceByKeyAndWindow(Integer::sum, new Duration(30000),new Duration(10000));

        rd.dstream().saveAsTextFiles("redditData","");
        typeCount.dstream().saveAsTextFiles("typeCount","");
        typeCount.print();
    }
}
