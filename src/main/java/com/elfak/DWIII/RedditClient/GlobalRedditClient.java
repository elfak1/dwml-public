package com.elfak.DWIII.RedditClient;

import net.dean.jraw.RedditClient;
import net.dean.jraw.http.NetworkAdapter;
import net.dean.jraw.http.OkHttpNetworkAdapter;
import net.dean.jraw.http.UserAgent;
import net.dean.jraw.oauth.Credentials;
import net.dean.jraw.oauth.OAuthHelper;
import org.springframework.beans.factory.annotation.Autowired;

import static com.elfak.DWIII.RedditClient.Constants.*;

public class GlobalRedditClient {

    private static RedditClient globalClient;
    public synchronized static RedditClient getClient(){
        if(globalClient == null){
            String platform = map.get(PLATFORM);
            String api_id = map.get(APP_ID);
            String version = map.get(VERSION);
            String redditUsername = map.get(REDDIT_USERNAME);
            UserAgent userAgent = new UserAgent(platform, api_id, version, redditUsername);
            String password = map.get(PASSWORD);
            String clientId = map.get(CLIENT_ID);
            String clientSecret = map.get(CLIENT_SECRET);
            Credentials credentials = Credentials.script(redditUsername, password, clientId, clientSecret);
            NetworkAdapter adapter = new OkHttpNetworkAdapter(userAgent);
            globalClient = OAuthHelper.automatic(adapter, credentials);
        }

        return globalClient;
    }
}
