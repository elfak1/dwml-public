package com.elfak.DWIII.RedditClient;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

@Configuration
@ConfigurationProperties(prefix = "application")
public class Constants implements Serializable {

    public static final String PLATFORM = "PLATFORM";
    public static final String APP_ID = "APP_ID" ;
    public static final String VERSION = "VERSION" ;
    public static final String REDDIT_USERNAME = "REDDIT_USERNAME";

    public static final String PASSWORD = "PASSWORD";
    public static final String CLIENT_ID = "CLIENT_ID" ;
    public static final String CLIENT_SECRET = "CLIENT_SECRET";

    public static final String TARGET_SUBREDDIT = "TARGET_SUBREDDIT";

    public static Map<String,String> map;

    public Constants() throws IOException {
        Properties properties = new Properties();
        properties.load(getClass().getClassLoader().getResourceAsStream("application.properties"));

        map = new HashMap<String, String>();
        map.put(PLATFORM,properties.getProperty("application.PLATFORM"));
        map.put(APP_ID,properties.getProperty("application.APP_ID"));
        map.put(VERSION,properties.getProperty("application.VERSION"));
        map.put(REDDIT_USERNAME,properties.getProperty("application.REDDIT_USERNAME"));
        map.put(PASSWORD,properties.getProperty("application.PASSWORD"));
        map.put(CLIENT_ID,properties.getProperty("application.CLIENT_ID"));
        map.put(CLIENT_SECRET,properties.getProperty("application.CLIENT_SECRET"));
        map.put(TARGET_SUBREDDIT,properties.getProperty("application.TARGET_SUBREDDIT"));


    }


}
