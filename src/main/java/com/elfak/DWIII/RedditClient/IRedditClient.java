package com.elfak.DWIII.RedditClient;

import net.dean.jraw.models.Submission;
import org.apache.spark.streaming.api.java.JavaDStream;

import java.io.Serializable;
import java.util.List;

public interface IRedditClient {

    public void getStream() throws InterruptedException;
}
