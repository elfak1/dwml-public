package com.elfak.DWIII;

import net.dean.jraw.models.Submission;
import org.apache.spark.storage.StorageLevel;
import org.apache.spark.streaming.StreamingContext;
import org.apache.spark.streaming.api.java.JavaInputDStream;
import org.apache.spark.streaming.api.java.JavaStreamingContext;
import org.apache.spark.streaming.dstream.InputDStream;
import org.apache.spark.streaming.dstream.ReceiverInputDStream;
import org.apache.spark.streaming.receiver.Receiver;
import scala.reflect.ClassTag;

public class RedditInputDReceiver extends ReceiverInputDStream<Submission> {


    public RedditInputDReceiver(JavaStreamingContext _ssc, ClassTag evidence$1) {
        super(_ssc.ssc(), evidence$1);
    }

    @Override
    public Receiver<Submission> getReceiver() {
        return new RedditReceiver(StorageLevel.MEMORY_AND_DISK());
    }
}
