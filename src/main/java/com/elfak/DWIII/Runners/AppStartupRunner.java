package com.elfak.DWIII.Runners;


import com.elfak.DWIII.RedditClient.IRedditClient;
import net.dean.jraw.models.Submission;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.streaming.api.java.JavaDStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.io.Serializable;


@Component
public class AppStartupRunner implements ApplicationRunner {
    private static final Logger logger = LoggerFactory.getLogger(AppStartupRunner.class);

    @Autowired
    IRedditClient redditClient;
    @Override
    public void run(ApplicationArguments args) throws Exception {

        redditClient.getStream();

    }
}